package ru.vmaksimenkov.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.repository.dto.IUserRecordRepository;
import ru.vmaksimenkov.tm.dto.UserRecord;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRecordRepository extends AbstractRecordRepository<UserRecord> implements IUserRecordRepository {

    public UserRecordRepository(@NotNull final EntityManager em) {
        super(em, UserRecord.class);
    }

    @Override
    public void clear() {
        em.createQuery("DELETE UserRecord").executeUpdate();
    }

    @Override
    public boolean existsByEmail(@Nullable final String email) {
        return em.createQuery("SELECT COUNT(*) FROM UserRecord WHERE email = :email", Long.class)
                .setParameter("email", email)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return em.createQuery("SELECT COUNT(*) FROM UserRecord WHERE id = :id", Long.class)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsByLogin(@Nullable final String login) {
        return em.createQuery("SELECT COUNT(*) FROM UserRecord WHERE login = :login", Long.class)
                .setParameter("login", login)
                .getSingleResult() > 0;
    }

    @NotNull
    @Override
    public List<UserRecord> findAll() {
        return em.createQuery("FROM UserRecord", UserRecord.class)
                .setHint(QueryHints.HINT_CACHEABLE, true).getResultList();
    }

    @Nullable
    @Override
    public UserRecord findByLogin(@Nullable final String login) {
        return getEntity(em.createQuery("FROM UserRecord WHERE login = :login", UserRecord.class)
                .setParameter("login", login)
                .setMaxResults(1));
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        em.createQuery("DELETE UserRecord WHERE login = :login")
                .setParameter("login", login).executeUpdate();
    }

    @Override
    public Long size() {
        return em.createQuery("SELECT COUNT(*) FROM UserRecord", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true).setMaxResults(1).getSingleResult();
    }

}
