package ru.vmaksimenkov.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.hibernate.mapping.PrimaryKey;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.repository.dto.IProjectRecordRepository;
import ru.vmaksimenkov.tm.dto.ProjectRecord;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectRecordRepository extends AbstractBusinessRecordRepository<ProjectRecord> implements IProjectRecordRepository {

    public ProjectRecordRepository(@NotNull final EntityManager em) {
        super(em, ProjectRecord.class);
    }

    @Override
    public void clear(@Nullable final String userId) {
        em.createQuery("DELETE ProjectRecord where userId = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void clear() {
        em.createQuery("DELETE ProjectRecord").executeUpdate();
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return em.createQuery("SELECT COUNT(*) FROM ProjectRecord WHERE userId = :userId AND id = :id", Long.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return em.createQuery("SELECT COUNT(*) FROM ProjectRecord WHERE id = :id", Long.class)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsByName(@Nullable final String userId, @Nullable final String name) {
        return em.createQuery("SELECT COUNT(*) FROM ProjectRecord WHERE userId = :userId AND name = :name", Long.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getSingleResult() > 0;
    }

    @NotNull
    @Override
    public List<ProjectRecord> findAll(@Nullable final String userId) {
        return em.createQuery("FROM ProjectRecord WHERE userId = :userId", ProjectRecord.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectRecord> findAll() {
        return em.createQuery("FROM ProjectRecord", ProjectRecord.class)
                .setHint(QueryHints.HINT_CACHEABLE, true).getResultList();
    }

    @Nullable
    @Override
    public ProjectRecord findById(@Nullable final String userId, @Nullable final String id) {
        return getEntity(em.createQuery("FROM ProjectRecord WHERE userId = :userId AND id = :id", ProjectRecord.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @Nullable
    @Override
    public ProjectRecord findByName(@Nullable final String userId, @Nullable final String name) {
        return getEntity(em.createQuery("FROM ProjectRecord WHERE userId = :userId AND name = :name", ProjectRecord.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1));
    }

    @Nullable
    @Override
    public String getIdByName(@Nullable final String userId, @Nullable final String name) {
        return em.createQuery("SELECT id FROM ProjectRecord WHERE userId = :userId AND name = :name", String.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        em.createQuery("DELETE FROM ProjectRecord WHERE userId = :userId AND name = :name")
                .setParameter("userId", userId)
                .setParameter("name", name).executeUpdate();
    }

    @NotNull
    @Override
    public Long size(@Nullable final String userId) {
        return em.createQuery("SELECT COUNT(*) FROM ProjectRecord WHERE userId = :userId", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public Long size() {
        return em.createQuery("SELECT COUNT(*) FROM ProjectRecord", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true).setMaxResults(1).getSingleResult();
    }

}
