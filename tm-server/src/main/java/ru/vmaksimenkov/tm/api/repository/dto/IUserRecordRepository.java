package ru.vmaksimenkov.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.dto.UserRecord;

public interface IUserRecordRepository extends IAbstractRecordRepository<UserRecord> {

    boolean existsByEmail(@Nullable String email);

    boolean existsByLogin(@Nullable String login);

    @Nullable
    UserRecord findByLogin(@Nullable String login);

    void removeByLogin(@Nullable String login);

}
