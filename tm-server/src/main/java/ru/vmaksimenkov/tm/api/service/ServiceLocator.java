package ru.vmaksimenkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.api.service.dto.*;
import ru.vmaksimenkov.tm.api.service.model.IProjectTaskService;
import ru.vmaksimenkov.tm.api.service.model.IUserService;

public interface ServiceLocator {

    @NotNull IAuthService getAuthService();

    @NotNull IConnectionService getConnectionService();

    @NotNull IProjectRecordService getProjectService();

    @NotNull IProjectTaskRecordService getProjectTaskService();

    @NotNull IProjectTaskService getProjectTaskServiceGraph();

    @NotNull IPropertyService getPropertyService();

    @NotNull ISessionRecordService getSessionService();

    @NotNull ITaskRecordService getTaskService();

    @NotNull IUserRecordService getUserService();

    @NotNull IUserService getUserServiceGraph();

}
