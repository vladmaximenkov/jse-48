package ru.vmaksimenkov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.api.service.IConnectionService;
import ru.vmaksimenkov.tm.api.service.model.IAbstractService;
import ru.vmaksimenkov.tm.model.AbstractBusinessEntity;

public abstract class AbstractService<E extends AbstractBusinessEntity> implements IAbstractService<E> {

    @NotNull
    private IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
