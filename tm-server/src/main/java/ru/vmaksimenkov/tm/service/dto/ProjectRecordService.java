package ru.vmaksimenkov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.repository.dto.IProjectRecordRepository;
import ru.vmaksimenkov.tm.api.service.IConnectionService;
import ru.vmaksimenkov.tm.api.service.dto.IProjectRecordService;
import ru.vmaksimenkov.tm.dto.ProjectRecord;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.exception.empty.EmptyIdException;
import ru.vmaksimenkov.tm.exception.empty.EmptyNameException;
import ru.vmaksimenkov.tm.exception.entity.ProjectNotFoundException;
import ru.vmaksimenkov.tm.exception.system.IndexIncorrectException;
import ru.vmaksimenkov.tm.repository.dto.ProjectRecordRepository;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

import static ru.vmaksimenkov.tm.util.ValidationUtil.checkIndex;
import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public class ProjectRecordService extends AbstractRecordService<ProjectRecord> implements IProjectRecordService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectRecordService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
        this.connectionService = connectionService;
    }

    @SneakyThrows
    public ProjectRecord add(@Nullable final ProjectRecord project) {
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(em);
            repository.add(project);
            em.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public ProjectRecord add(@NotNull final String userId, @Nullable final String name, @Nullable final String description) {
        if (isEmpty(name)) throw new EmptyNameException();
        @NotNull final ProjectRecord project = new ProjectRecord();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        add(project);
        return project;
    }

    @SneakyThrows
    public void add(@Nullable final List<ProjectRecord> list) {
        if (list == null) throw new ProjectNotFoundException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(em);
            repository.add(list);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(em);
            repository.clear(userId);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(em);
            repository.clear();
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(em);
            return repository.existsById(id);
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(em);
            return repository.existsById(userId, id);
        } finally {
            em.close();
        }
    }

    @SneakyThrows
    public boolean existsByName(@NotNull final String userId, @Nullable final String name) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(em);
            return repository.existsByName(userId, name);
        } finally {
            em.close();
        }
    }

    @Nullable
    @SneakyThrows
    public List<ProjectRecord> findAll(@NotNull final String userId, @NotNull final Comparator<ProjectRecord> comparator) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(em);
            return repository.findAll(userId);
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectRecord> findAll(@Nullable final String userId) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(em);
            return repository.findAll(userId);
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectRecord> findAll() {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(em);
            return repository.findAll();
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectRecord findById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(em);
            return repository.findById(userId, id);
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectRecord findById(@NotNull final String id) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(em);
            return repository.findById(id);
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectRecord findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(em);
            return repository.findByIndex(userId, index);
        } finally {
            em.close();
        }
    }

    @Override
    public @Nullable ProjectRecord findByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(em);
            return repository.findByName(userId, name);
        } finally {
            em.close();
        }
    }

    @Override
    public void finishProjectById(@NotNull final String userId, @Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @Nullable final ProjectRecord project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        update(project);
    }

    @Override
    public void finishProjectByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (!checkIndex(index, size(userId))) throw new IndexIncorrectException();
        @Nullable final ProjectRecord project = findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        update(project);
    }

    @Override
    public void finishProjectByName(@NotNull final String userId, @Nullable final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final ProjectRecord project = findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        update(project);
    }

    @Nullable
    @Override
    public String getIdByIndex(@Nullable final String userId, @Nullable final Integer index) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(em);
            return repository.getIdByIndex(userId, index);
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final ProjectRecord project) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(em);
            repository.remove(project);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(em);
            repository.removeById(id);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(em);
            repository.removeById(userId, id);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(em);
            repository.removeByIndex(userId, index);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void setProjectStatusById(@NotNull final String userId, @Nullable final String id, @NotNull final Status status) {
        if (isEmpty(id)) throw new EmptyIdException();
        @Nullable final ProjectRecord project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        update(project);
    }

    @Override
    public void setProjectStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        if (!checkIndex(index, size(userId))) throw new IndexIncorrectException();
        @Nullable final ProjectRecord project = findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        update(project);
    }

    @Override
    public void setProjectStatusByName(@NotNull final String userId, @Nullable final String name, @NotNull final Status status) {
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final ProjectRecord project = findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        update(project);
    }

    @Override
    @SneakyThrows
    public Long size() {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(em);
            return repository.size();
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Long size(@Nullable final String userId) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(em);
            return repository.size(userId);
        } finally {
            em.close();
        }
    }

    @Override
    public void startProjectById(@NotNull final String userId, @Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @Nullable final ProjectRecord project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        update(project);
    }

    @Override
    public void startProjectByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final ProjectRecord project = findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        update(project);
    }

    @Override
    public void startProjectByName(@NotNull final String userId, @Nullable final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final ProjectRecord project = findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        update(project);
    }

    @SneakyThrows
    public void update(@Nullable final ProjectRecord project) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(em);
            repository.update(project);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void updateProjectById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(id)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final ProjectRecord project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        update(project);
    }

    @Override
    public void updateProjectByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (!checkIndex(index, size(userId))) throw new IndexIncorrectException();
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final ProjectRecord project = findByIndex(userId, index - 1);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        update(project);
    }

    @Override
    public void updateProjectByName(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String nameNew,
            @Nullable final String description
    ) {
        if (isEmpty(name) || isEmpty(nameNew)) throw new EmptyNameException();
        @Nullable final ProjectRecord project = findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(nameNew);
        project.setDescription(description);
        update(project);
    }

}
