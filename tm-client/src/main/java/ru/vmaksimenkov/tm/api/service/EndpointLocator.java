package ru.vmaksimenkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.endpoint.*;

public interface EndpointLocator {

    @NotNull AdminEndpoint getAdminEndpoint();

    @NotNull AdminUserEndpoint getAdminUserEndpoint();

    @NotNull ICommandService getCommandService();

    @NotNull ProjectEndpoint getProjectEndpoint();

    @NotNull IPropertyService getPropertyService();

    @Nullable SessionRecord getSession();

    void setSession(@Nullable SessionRecord session);

    @NotNull SessionEndpoint getSessionEndpoint();

    @NotNull TaskEndpoint getTaskEndpoint();

    @NotNull UserEndpoint getUserEndpoint();

}
