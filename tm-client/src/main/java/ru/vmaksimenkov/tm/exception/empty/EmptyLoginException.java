package ru.vmaksimenkov.tm.exception.empty;

import ru.vmaksimenkov.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error! Login is empty...");
    }

}
