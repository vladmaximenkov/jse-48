package ru.vmaksimenkov.tm.exception.empty;

import ru.vmaksimenkov.tm.exception.AbstractException;

public class EmptyEmailException extends AbstractException {

    public EmptyEmailException() {
        super("Error! E-mail is empty...");
    }

}
