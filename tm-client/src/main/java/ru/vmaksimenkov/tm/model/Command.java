package ru.vmaksimenkov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class Command {

    @Nullable
    private String arg = "";

    @Nullable
    private String description = "";

    @Nullable
    private String name = "";

    public Command(@Nullable final String name, @Nullable final String arg, @Nullable final String description) {
        this.name = name;
        this.arg = arg;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (arg != null && !arg.isEmpty()) result += " [" + arg + "]";
        if (description != null && !description.isEmpty()) result += " - " + description;
        return result;
    }

}
